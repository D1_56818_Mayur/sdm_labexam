const mysql=require("mysql2")

const openConnection=()=>{
    const connection=mysql.createConnection({
        host: "db",
        user: "root",
        password: "root",
        database: "mydb",
        waitForConnections: true
    })

    connection.connect()
    return connection
}

module.exports={
    openConnection,
}
