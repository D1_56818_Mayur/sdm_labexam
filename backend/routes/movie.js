const express=require('express')
const db=require('../db')

const router=express.Router()

router.get('/getMovie/:name',(request,response)=>{
    const movie_title=request.params.name
    const statement=`select * from movie where movie_title='${movie_title}'`
    const connection=db.openConnection()

    connection.query(statement,(error,result)=>{
        if(error){
            response.send(error)
        }else{
            response.send(result)
        }
    })
})

router.post('/addMovie/',(request,response)=>{
    const {movie_title,movie_release_date,movie_time,director_name}=request.body
    const statement=`insert into movie (movie_title,movie_release_date,movie_time,director_name) values ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`
    const connection=db.openConnection()

    connection.query(statement,(error,result)=>{
        if(error){
            response.send(error)
        }else{
            response.send(result)
        }
    })
})

router.put('/updateMovie/',(request,response)=>{
    const {movie_id, movie_release_date, movie_time}=request.body
    const statement=`select * from movie where movie_id=${movie_id}`
    const connection=db.openConnection()

    connection.query(statement,(error,result)=>{
        if(error){
            response.send(error)
        }else{
            if(result.length==0){
                response.send('no movie found for given id')
            }else{
                const statement1=`update movie set movie_release_date='${movie_release_date}', movie_time='${movie_time}' where movie_id=${movie_id}`
                connection.query(statement1,(error,data)=>{
                    if(error){
                        response.send(error)
                    }else{
                        response.send(data)
                    }
                })
            }
        }
    })
})

router.delete('/deleteMovie/:id',(request,response)=>{
    const movie_id=request.params.id
    const statement=`delete from movie where movie_id='${movie_id}'`
    const connection=db.openConnection()

    connection.query(statement,(error,result)=>{
        if(error){
            response.send(error)
        }else{
            response.send(result)
        }
    })
})

module.exports=router