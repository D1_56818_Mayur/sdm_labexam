create table movie(
    movie_id integer primary key auto_increment,
    movie_title varchar(200),
    movie_release_date date,
    movie_time TIME, 
    director_name varchar(200)
);

insert into movie (movie_title,movie_release_date,movie_time,director_name) values ('kika','2021-11-23','12:00:00','mohit');